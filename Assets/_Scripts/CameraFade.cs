﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class CameraFade : Singleton<CameraFade> {

    public static CameraFade Instance
    {
        get { return (CameraFade)mInstance; }
        set { mInstance = value; }
    }

    public Material Mat_Fader;
    public bool fadeOnStart;

    void Start()
    {
        if (fadeOnStart)
        {
            Mat_Fader.SetFloat("_Alpha", 1);
            FadeOut();
        }
    }

    public void FadeIn()
    {
       Mat_Fader.DOFloat(1,"_Alpha",1.5f);
    }

    public void FadeOut()
    {
        Mat_Fader.DOFloat(0, "_Alpha", 1.5f);
    }

    public void FadeIn(Action onComplete)
    {
        Mat_Fader.DOFloat(1, "_Alpha", 1.5f).OnComplete(() =>onComplete());
    }

}
