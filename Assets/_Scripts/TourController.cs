﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TourController : MonoBehaviour {

    public GameObject listItem_Scenes;
    public Material mainPanoMaterial;
    public GameObject pointObj;
    public string testPathOfTour;
    public int openedPoints = 0;
    public PanoFade panoFade;

    private TourView _tourView;
    private TourSceneView _currentSceneView;
    private GameObject _selectedSceneButton;
    private InteractivePointView _currentInteractivePointView;
    private List<GameObject> _inScenePointObjsList;

    //Need to change (connect with interface)
    void Start()
    {
        _tourView = AppData.Instance.tourView;
        _inScenePointObjsList = new List<GameObject>();
        if (_tourView != null) FillTheSceneList();

    }

    public void FillTheSceneList()
    {
        for (int i = 0; i < _tourView.TourScenes.Count; i++)
        {
            GameObject item = Instantiate(listItem_Scenes, Vector3.zero, Quaternion.identity) as GameObject;
            item.transform.SetParent(listItem_Scenes.transform.parent);
            item.transform.GetChild(0).GetComponent<RawImage>().texture = _tourView.TourScenes[i].panorama;
            item.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = _tourView.TourScenes[i].Name;
            int index = i;
            item.GetComponent<Button>().onClick.AddListener(() => {
                _currentSceneView = _tourView.TourScenes[index];
                CameraFade.Instance.FadeIn(()=>{
                    SwitchToScene(index);
                    CameraFade.Instance.FadeOut();
                });
            });
            item.transform.localPosition = listItem_Scenes.transform.localPosition;
            item.transform.localRotation = listItem_Scenes.transform.localRotation;
            item.transform.localScale = listItem_Scenes.transform.localScale;
            item.SetActive(true);
            
        }
        _currentSceneView = _tourView.TourScenes[0];
        SwitchToScene(0);
    }

    #region Scene
    public void SwitchToScene(int index)
    {
        openedPoints = 0;
        if (panoFade.isFocuesd) panoFade.Unfocus();
        _currentSceneView = _tourView.TourScenes[index];
        mainPanoMaterial.SetTexture("_MainTex",_currentSceneView.panorama);
        //LocateExistingPoints();
    }
    #endregion

    #region Interactive Point
    public void LocateExistingPoints()
    {
       DestroyAllPointsInScene();
       TourSceneView currentScene=_tourView.TourScenes.Find(x=>x==_currentSceneView);
       for (int i = 0; i < currentScene.InteractivePoints.Count; i++)
       {
           Vector3 pointPosition = currentScene.InteractivePoints[i].Position;
           GameObject item = Instantiate(pointObj, pointPosition, Quaternion.identity) as GameObject;
       
           item.transform.LookAt(2*pointPosition-Camera.main.transform.position);
           item.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = currentScene.InteractivePoints[i].image;
           item.transform.GetChild(0).GetComponent<Canvas>().worldCamera = Camera.main;
           _inScenePointObjsList.Add(item);
       }
    }

    public void DestroyAllPointsInScene()
    {
        for (int i = 0; i < _inScenePointObjsList.Count; i++)
        {
            Destroy(_inScenePointObjsList[i]);
        }
        _inScenePointObjsList.Clear();
    }
    #endregion

    public void SwitchFocus()
    {
        if (openedPoints > 0 && !panoFade.isFocuesd)
        {
            panoFade.Focus();
        }
        else if (openedPoints == 0 && panoFade.isFocuesd) panoFade.Unfocus();
    }
}
