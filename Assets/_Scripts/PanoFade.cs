﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PanoFade : MonoBehaviour {

    public Material Mat_Fader;
    public bool isFocuesd = false;
    public float time=0.4f;
    public float focusIntensity = 0.5f;

    public void Focus()
    {
        Mat_Fader.DOFloat(focusIntensity, "_Alpha", time);
        isFocuesd = true;
    }

    public void Unfocus()
    {
        Mat_Fader.DOFloat(0, "_Alpha", time);
        isFocuesd = false;
    }

    public void Focus(Action onComplete)
    {
        Mat_Fader.DOFloat(1, "_Alpha", 1.5f).OnComplete(() => onComplete());
        isFocuesd = true;
    }

    
}
