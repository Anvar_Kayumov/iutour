﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class SwipeRotater : MonoBehaviour {

    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    ///чувствительность
    public float sensitivityX = 50F;
    public float sensitivityY = 50F;
    ///множитель торможения остановки инерционной составляющей. Чем больше тем быстрее гаситься инерция.
    public float StopedMnojitel = 10F;
    ///Блокировки для осей. По Х можно кругом, а по y - 60 градусов наверх и столько же вниз.
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    float rotationX = 0F;
    float rotationY = 0F;
    bool canRotate = false;
    Quaternion originalRotation;
    ///инерционная
    public Vector2 velocity = Vector2.zero;


    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            rotationX = transform.localEulerAngles.y;
            if (transform.localEulerAngles.x <= 85) rotationY = -transform.localEulerAngles.x;
            else { rotationY = 360 - transform.localEulerAngles.x; }
            LetsCentralizeCamera = false;
            canRotate = !EventSystem.current.IsPointerOverGameObject();
        }
        else if (Input.GetMouseButtonUp(0)) canRotate = false;
        if (axes == RotationAxes.MouseXAndY)
        {
            if (canRotate)
            {
                velocity.x -= Input.GetAxis("Mouse X") * sensitivityX;
                velocity.y -= Input.GetAxis("Mouse Y") * sensitivityY;
            }
            // Read the mouse input axis
            rotationX += velocity.x * Time.deltaTime;
            rotationY += velocity.y * Time.deltaTime;
            //общая инерционная, которая гасится StopedMnojitel
            velocity = velocity - ((velocity * Time.deltaTime) * StopedMnojitel);
            rotationX = ClampAngle(rotationX, minimumX, maximumX);
            rotationY = ClampAngle(rotationY, minimumY, maximumY);
         
            Quaternion xQuaternion = Quaternion.AngleAxis(rotationX, Vector3.up);
            Quaternion yQuaternion = Quaternion.AngleAxis(rotationY, Vector3.left);
            if (LetsCentralizeCamera) transform.localRotation = Quaternion.Slerp(transform.localRotation,Quaternion.Euler(0,0,0), 4*Time.deltaTime);
            else transform.localRotation = originalRotation * xQuaternion * yQuaternion;
           
               
        }
    }
    private bool LetsCentralizeCamera = false;
    public void Centralize()
    {
      //  rotationY = 0;
       // rotationX = 0;
        LetsCentralizeCamera = true;
     
    }
    void Start()
    {
        // Make the rigid body not change rotation
       
        originalRotation = transform.localRotation;
    }
    ///Блокировки врнащений   
    public static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360F)
            angle += 360F;
        if (angle > 360F)
            angle -= 360F;
        return Mathf.Clamp(angle, min, max);
    }
}
