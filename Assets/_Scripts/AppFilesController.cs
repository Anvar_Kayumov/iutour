﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AppFilesController : MonoBehaviour {

    private string[] folders = new string[] { "Tours" };

    private string _path;

    void Awake()
    {
        foreach (string folder in folders)
        {
            if(Application.platform==RuntimePlatform.Android)
                _path="/storage/emulated/0/";
            else _path=Application.dataPath+"/../";

            if (!Directory.Exists(_path + folder))
            {
                Directory.CreateDirectory(_path + folder);
            }
        }
    }
}
