﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Mode
{
    NotVR,
    Cardboard,
    GearVR
}

public class AppData : Singleton<AppData>
{
    public static AppData Instance
    {
        get
        {
            return ((AppData)mInstance);
        }
        set
        {
            mInstance = value;
        }
    }

    public TourView tourView;
    public Mode viewMode;
    public string currentTourPath;
    private string _toursPath;

    void Awake()
    {
        if (Application.platform == RuntimePlatform.Android) _toursPath = "/storage/emulated/0/Tours/";
        else _toursPath = Application.dataPath + "/../" + "Tours/";
    }
    public void GetTourFromFile(string tourName)
    {
        currentTourPath = _toursPath + tourName;
        TourModel tourModel = BinaryController.DeSerializeTour(currentTourPath);
        if (tourModel != null)
        {
            tourView = new TourView();
            tourView.Name = tourModel.Name;

            for (int i = 0; i < tourModel.TourScenes.Count; i++)
            {
                TourSceneModel tourSceneModel = tourModel.TourScenes[i];
                TourSceneView tourSceneview = new TourSceneView();
                tourSceneview.Name = tourSceneModel.Name;
                tourSceneview.audio = null;
                Texture2D pano = new Texture2D(2, 2);
                pano.LoadImage(tourSceneModel.PanoData);
                tourSceneview.panorama = pano;

                for (int j = 0; j < tourSceneModel.InteractivePoints.Count; j++)
                {
                    InteractivePointModel interactivePointModel = tourSceneModel.InteractivePoints[j];
                    InteractivePointView interactivePointView = new InteractivePointView();

                    //Get Image from bytes
                    Texture2D tex = new Texture2D(2, 2);
                    tex.LoadImage(interactivePointModel.Content.Data);
                    interactivePointView.image = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.0f, 0.0f));
                    PointPosition pointPosition = interactivePointModel.Position;
                    interactivePointView.Position = new Vector3(pointPosition.x, pointPosition.y, pointPosition.z);
                    tourSceneview.InteractivePoints.Add(interactivePointView);
                }
                tourView.TourScenes.Add(tourSceneview);
            }
            Debug.Log("Deserialized Succesfully");
        }
    }

  
}

#region TourModel for Serialization
[Serializable]
public class TourModel
{
    public TourModel()
    {
        TourScenes = new List<TourSceneModel>();
    }
    public string Name { get; set; }
    public List<TourSceneModel> TourScenes { get; set; }
}

[Serializable]
public class TourSceneModel
{
    public TourSceneModel()
    {
        InteractivePoints = new List<InteractivePointModel>();
    }
    public string Name { get; set; }
    public byte[] PanoData { get; set; }
    public byte[] AudioData { get; set; }
    public List<InteractivePointModel> InteractivePoints { get; set; }
}

//We have to change Vector3 to distance from camera
[Serializable]
public class InteractivePointModel
{
    public ContentImageModel Content { get; set; }
    public PointPosition Position { get; set; }
}

[Serializable]
public struct ContentImageModel
{
    public byte[] Data { get; set; }
}
#endregion

#region TourView
public class TourView
{
    public TourView()
    {
        TourScenes = new List<TourSceneView>();
    }
    public string Name { get; set; }
    public List<TourSceneView> TourScenes { get; set; }
}
public class TourSceneView
{
    public TourSceneView()
    {
        InteractivePoints = new List<InteractivePointView>();
    }
    public string Name { get; set; }
    public Texture2D panorama { get; set; }
    public AudioClip audio { get; set; }
    public List<InteractivePointView> InteractivePoints { get; set; }
}
//We have to change Vector3 to distance from camera
public class InteractivePointView
{
    public Sprite image;
    public Vector3 Position { get; set; }
}
#endregion

[Serializable]
public struct PointPosition
{
    public float x { get; set; }
    public float y { get; set; }
    public float z { get; set; }
}