﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{

    public string tourName;

    private string _currentTourName;

    private void Start()
    {
        OpenTour(tourName);
    }

    public void OpenTour(string tourName)
    {
        _currentTourName = tourName;
        StartCoroutine(OpenTourAsync());
    }
    IEnumerator OpenTourAsync()
    {
        yield return new WaitForSeconds(0.5f);
        AppData.Instance.GetTourFromFile(_currentTourName);
        CameraFade.Instance.FadeIn(() => { SceneManager.LoadSceneAsync("INHAcurrentBuilding"); });    
    }

}
