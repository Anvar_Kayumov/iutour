﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class UI_Point : MonoBehaviour {

    public Image Image_Point;
    public Image Image_Content;
    private TourController _tourController;
    private PanoFade _panoFade;

    void Start()
    {
        _tourController = FindObjectOfType<TourController>() as TourController;
        _panoFade = FindObjectOfType<PanoFade>() as PanoFade;
    }

    public void ShowContent()
    {
        _tourController.openedPoints++;
        _tourController.SwitchFocus();
        Image_Point.raycastTarget = false;
        Image_Content.raycastTarget = true;
        Image_Point.transform.DOScale(0, 0.2f).OnComplete(() =>
        {
            Image_Content.transform.DOScale(3, 0.2f);
        });
    }
    
    public void HideContent()
    {
        _tourController.openedPoints--;
        _tourController.SwitchFocus();
        Image_Point.raycastTarget = true;
        Image_Content.raycastTarget = false;
        Image_Content.transform.DOScale(0, 0.2f).OnComplete(() => {
            Image_Point.transform.DOScale(0.6f, 0.2f);
        });
    }
}
