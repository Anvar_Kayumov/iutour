﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UI_TourItem : MonoBehaviour {

    public Transform mainImage;
    public string tourName;
    
    public void OnPointEnter()
    {
        mainImage.DOScale(0.9f,0.6f);
    }

    public void OnPointExit()
    {
        mainImage.DOScale(1f, 0.6f);
    }

    public void OpenTour()
    { 
    
    }
    

}
