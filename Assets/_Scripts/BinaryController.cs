﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class BinaryController {

    private static string _savePath = Application.dataPath + "/../" + "Tours/";
    public static byte[] GetAudioClipFromBytes(byte[] audioData)
    {
        return null;
    }

    public static void SerializeTour(TourModel tourModel)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        // получаем поток, куда будем записывать сериализованный объект
        using (FileStream fs = new FileStream(_savePath+tourModel.Name+".tour", FileMode.OpenOrCreate))
        {
            formatter.Serialize(fs, tourModel);
        }
    }
    public static TourModel DeSerializeTour(string path)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        TourModel tour;
        using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
        {
            tour = (TourModel)formatter.Deserialize(fs);
        }
        return tour;
    }
}
